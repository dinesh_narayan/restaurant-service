package com.foodApp.restaurantservice;

import com.foodApp.restaurantservice.model.Item;
import com.foodApp.restaurantservice.model.Restaurant;
import com.foodApp.restaurantservice.repo.RestaurantRepo;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoClientDbFactory;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import java.util.Arrays;
import java.util.List;

@SpringBootApplication
@EnableMongoRepositories(basePackages = "com.foodApp.restaurantservice.repo")
public class RestaurantServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestaurantServiceApplication.class, args);
	}



	//@Bean
	public CommandLineRunner commandLineRunner(RestaurantRepo repo) {
		List<Item> thalapItems = Arrays.asList(new Item("Biriyani","260"),
				new Item("Parotta","60"),
				new Item("Grill Chicken","298")
				);

		List<Item> kfcItems = Arrays.asList(new Item("Burger","199"),
				new Item("Krushers","90"),
				new Item("Hot and Spicy Chicken","180")
		);

		List<Item> wangsItems = Arrays.asList(new Item("Noodles","260"),
				new Item("Dragon Chicken","100"),
				new Item("Gobi Manchurian","190")
		);

		return string->{
			List<Restaurant> rest = Arrays.asList(new Restaurant("Thalapakatti",thalapItems),
					new Restaurant("KFC",kfcItems),
					new Restaurant("Wangs Kitchen",wangsItems));

			rest.stream().forEach((restaurant -> repo.save(restaurant)));
		};
	}

}
