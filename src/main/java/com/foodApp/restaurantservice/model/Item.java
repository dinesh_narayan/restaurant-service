package com.foodApp.restaurantservice.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Getter@Setter
public class Item {

    @JsonProperty("Item ID")
    private String id;
    private String itemName;

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    private String itemCost;

    public String getItemCost() {
        return itemCost;
    }

    public void setItemCost(String itemCost) {
        this.itemCost = itemCost;
    }

    @JsonCreator
    public Item(@JsonProperty("Item Name") String itemName,@JsonProperty("Item Cost") String itemCost) {

        this.itemName = itemName;
        this.itemCost = itemCost;
    }
}
