package com.foodApp.restaurantservice.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.annotation.Generated;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
@Getter@Setter
@Document(collection = "restaurant")
public class Restaurant{

    @Id
    @JsonProperty("Restaurant ID")
    private String id;
    private String restaurantName;
    private List<Item> items;

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    @JsonCreator
    public Restaurant(@JsonProperty("Restaurant Name") String restaurantName, @JsonProperty("Restaurant Items") List<Item> items) {
        this.restaurantName = restaurantName;
        this.items = items;
    }
}
