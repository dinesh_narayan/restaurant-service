package com.foodApp.restaurantservice.repo;

import com.foodApp.restaurantservice.model.Item;
import com.foodApp.restaurantservice.model.Restaurant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class MongoCustomDBImpl implements MongoCustomDB {

    @Autowired
    MongoTemplate mTemplate;

    @Override
    public List<Item> getItemsFromRestaurantID(String id) {
        System.out.println("For Commit");
        Query query = new Query();
        query.addCriteria(Criteria
        .where("_id").is(id));
         Restaurant rest = mTemplate.findOne(query, Restaurant.class);
         return rest.getItems().stream().sorted(
                 Comparator.comparing(Item::getItemName)
        ).collect(Collectors.toList());

        /*return rest.getItems().stream().sorted(
                (o1,o2)->{
                    return o1.getItemCost().compareTo(o2.getItemCost());
                }
        ).collect(Collectors.toList());*/

    }


}
