package com.foodApp.restaurantservice.repo;

import com.foodApp.restaurantservice.model.Restaurant;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RestaurantRepo extends MongoRepository<Restaurant, String>, MongoCustomDB {

    List<Restaurant> findAllByOrderByRestaurantNameAsc();

}
