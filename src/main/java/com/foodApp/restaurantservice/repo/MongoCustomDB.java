package com.foodApp.restaurantservice.repo;

import com.foodApp.restaurantservice.model.Item;

import java.util.List;

public interface MongoCustomDB {

    List<Item> getItemsFromRestaurantID(String id);
}
