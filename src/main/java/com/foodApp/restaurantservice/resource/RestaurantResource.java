package com.foodApp.restaurantservice.resource;

import com.foodApp.restaurantservice.model.Item;
import com.foodApp.restaurantservice.model.Restaurant;
import com.foodApp.restaurantservice.services.RestaurantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.print.attribute.standard.Media;
import java.util.List;

@RestController
@RequestMapping("/restaurant")
public class RestaurantResource {

    @Autowired
    private RestaurantService resturantService;

    //TO DO
    // 1. Get All Restaurants
    @GetMapping(path = "/view", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Restaurant> getAllRestaurant() {
        return resturantService.getAll();
    }
    // 2. Get Restaurant by Restaurant ID
    @GetMapping(path = "/view/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Restaurant getRestaurantByID(@PathVariable("id") String id) {
        return resturantService.getOneByID(id);
    }
    // 3. Get Items of Restaurant by Restaurant ID
    @GetMapping(path = "/items/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Item> getItemsByRestaurantID(@PathVariable("id") String id) {
        return resturantService.getItemsByRestaurantID(id);
    }


}
