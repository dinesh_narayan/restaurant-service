package com.foodApp.restaurantservice.services;

import com.foodApp.restaurantservice.model.Item;
import com.foodApp.restaurantservice.model.Restaurant;

import java.util.List;

public interface RestaurantService {


    List<Restaurant> getAll();

    Restaurant getOneByID(String id);

    List<Item> getItemsByRestaurantID(String id);
}
